import { readFileSync, writeFileSync, mkdirSync, cpSync} from "fs";
import { fileURLToPath } from 'url';
import { dirname } from 'path';

const path = dirname(dirname(fileURLToPath(import.meta.url)));
mkdirSync(`${path}/dist`, {recursive: true, force: true});

let indexContent = readFileSync(`${path}/src/index.html`, "utf-8")
    .replace(`url("ogcourier.woff2")`, `url("data:font/woff2;base64,${readFileSync(`${path}/src/ogcourier.woff2`).toString("base64")}")`)
    .replace('src="hellcat.gif"', `src="data:image/gif;base64,${readFileSync(`${path}/src/hellcat.gif`).toString("base64")}"`)
    .replace(`url("cursor.webp")`, `url("data:image/gif;base64,${readFileSync(`${path}/src/cursor.webp`).toString("base64")}")`)
    .replace(`src="qr.png"`, `src="data:image/gif;base64,${readFileSync(`${path}/src/qr.png`).toString("base64")}"`);
writeFileSync(`${path}/dist/index.html`, indexContent, {recursive: true, force: true});

cpSync(`${path}/src/robots.txt`, `${path}/dist/robots.txt`, {recursive: true, force: true});
console.log("build completed");
